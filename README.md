# Moodle Procrastination Graph

## Step 1: Download the Evaluation Form

On the moodle page of the TP:

1. Click on `Consulter tous les travaux remis`

2. On the top of the page, click on `Action d'évaluation`

3. Select `Télécharger le formulaire d'évaluation`

## Step 2: Update the required info in the script

You need to update the R script to add:

- the path to the evalution form

- the date of the deadline


```R
filename <- "./moodle.csv"
deadline <- ymd_hms("2021/10/24 23:59:59")
```

## Step 3: Run the script

To run the script:

```sh
Rscript procrastination.R
```

It will generate `.png` files with the procrastination graphs

## Proportion of submissions

![Proportion of Submissions](./examples/graphe_procrastination_proportion.png)

## Submission time per Group

![Submission per group](./examples/graphe_procrastination_groupes.png)
