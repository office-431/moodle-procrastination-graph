with import <nixpkgs> {};
mkShell {
	name = "shell";
	buildInputs = [
		R
		rPackages.tidyverse
		rPackages.lubridate
	];
}
